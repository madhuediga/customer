package com.customer.controller;

import java.util.List;


import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.customer.entity.Contact;
import com.customer.service.ContactService;


@RestController
@RequestMapping("/api/contact")
public class ContactRestController {
    

	private ContactService contactService;
	
	public ContactRestController(ContactService contactService) {
		this.contactService=contactService;
	}
	
	@PostMapping
	public ResponseEntity<String> saveContect(@RequestBody Contact contact){
		
		boolean isSaved = contactService.saveContact(contact);
		if(isSaved) {
			return new ResponseEntity<>("Contact saved", HttpStatus.CREATED);
		}
		return new ResponseEntity<>("Failed To save",HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@GetMapping
	public ResponseEntity<List<Contact>> getAllContacts(){
		List<Contact> allContacts=contactService.getAllContacts();
		return new ResponseEntity<>(allContacts,HttpStatus.OK);
				
	}
	
	@GetMapping("/{contactId}")
	public ResponseEntity<Contact> getContectById(Integer contactID){
		Contact contact=contactService.getContactById(contactID);
		
		return new ResponseEntity<Contact>(contact,HttpStatus.OK);
	}
	
	@DeleteMapping("/{contactId}")
	public ResponseEntity<String> deleteContectById(Integer contactID){
		boolean isDeleted =contactService.deleteContactById(contactID);
		if(isDeleted) { 
			return new ResponseEntity<>("Deleted",HttpStatus.OK);
		}else {
		
		       return new ResponseEntity<>("Failed",HttpStatus.INTERNAL_SERVER_ERROR);
	}}
	
	
}
