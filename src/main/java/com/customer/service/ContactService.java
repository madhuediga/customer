package com.customer.service;

import java.util.List;

import com.customer.entity.Contact;

public interface ContactService {
 
	public boolean saveContact(Contact contact);
	
	public List<Contact> getAllContacts();
	
	public Contact getContactById(Integer contactID);
	
	public boolean deleteContactById(Integer contactID);
}
