package com.customer.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.customer.entity.Contact;
import com.customer.repo.ContactRepositery;

@Service
public class ContectServiceImpl implements ContactService {

	@Autowired
	private ContactRepositery contactRepo;
	
	
	@Override
	public boolean saveContact(Contact contact) {
		Contact savedContact=contactRepo.save(contact);
		
		return savedContact.getContactID() !=null;
	}

	@Override
	public List<Contact> getAllContacts() {
	   
		return	contactRepo.findAll();
	}

	@Override
	public Contact getContactById(Integer contactID) {
		Optional<Contact> findById = contactRepo.findById(contactID);
		if(findById.isPresent()) {
			return findById.get();
		}
		return null;
	}

	@Override
	public boolean deleteContactById(Integer contactID) {
      try {
		contactRepo.deleteById(contactID);
		return true;
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
      return false;
	}

}
