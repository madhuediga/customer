package com.customer.repo;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.customer.entity.Contact;
public interface ContactRepositery extends JpaRepository<Contact, Serializable> {

}
