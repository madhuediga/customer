package com.customer.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Data
@Setter
@Getter
@Table(name="contact")
public class Contact {

	@Id
	@GeneratedValue
	@Column(name="CONTACT_ID")
	private Integer contactID;
	
	@Column(name="CONTACT_NAME")
	private String contactName;
	
	@Column(name="CONTACT_EMAIL")
	private String contactEmail;
	
	@Column(name="CONTACT_NUMBER")
	private String contactNumber;

	@Column(name = "Insurance_personName")
	private String insurancePersonName;

	@Column(name = "InsuranceType")
	private String insuranceType;

	@Column(name = "InsuranceBond")
	private String insuranceBond;

	@Column(name = "insurance_percentage")
	private Integer percentage;

	@Column(name = "Members_list")
	private Integer membersLIst;

	@Column(name = "Clamb_money")
	private Long money;

	@Column(name = "Insurance_applied_date")
	private Date insurance_applied_date;

	@Column(name = "Insurance_approved_date")
	private Date insurance_approved_date;

	@Column(name = "Insurance_Status")
	private String Insurance_Status;


	
}
